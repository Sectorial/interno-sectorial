<?php

namespace IAPOS\GeneralBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class PreliquidacionesController extends Controller {

    public function indexAction() {
        return $this->render('IAPOSGeneralBundle:Preliquidaciones:index.html.twig');
    }

    public function generarArchivoAction() {

        // Obtiene los parametros desde el request (GET) en la forma clasica de PHP
        $fecha = new \DateTime($_GET["fecha"]);
        $entidad = $_GET["entidad"];
        $entidadString = $_GET["entidadString"];
        $extensionArchivo = strtolower($_GET["extensionArchivo"]);
        $caracterSeparacion = $_GET["caracterSeparacion"];

        $preliquidacion = "";
        switch ($entidad) {
            case 2; // Circulo Kinesio SF
                $preliquidacion = $this->generarPreliquidacionGenerica($fecha, 759);
                break;
            case 3; // APRO
                $preliquidacion = $this->generarPreliquidacionGenerica($fecha, 7055);
                break;
            case 4; // Camara de Opticas
                $preliquidacion = $this->generarPreliquidacionGenerica($fecha, 1);
                break;
            default; // Gremial Medica | Asoc Prof Sangre
                $preliquidacion = $this->generarPreliquidacionParticular($fecha, $entidad);
                break;
        }

        // Genera el string para el nombre del archivo que se entregara del lado cliente
        $nombreArchivo = 'Preliquidación - ' . $entidadString . ' [' . $fecha->format("m-Y") . '].' . $extensionArchivo;

        // Content-Type
        $contentType = "";

        switch ($extensionArchivo) {
            case "xls":
                // Crea el writer
                $writer = $this->get('phpexcel')->createWriter($preliquidacion, 'Excel5');
                
                // Crea el response
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                
                // Se añaden las cabeceras
                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');
                $response->headers->set('Content-Disposition', 'attachment; filename="'.$nombreArchivo.'"');
                $response->headers->set('Set-Cookie', 'fileDownload=true; path=/');
                
                return $response;
                
                break;
            case "csv":
                // Crea el writer
                $writer = $this->get('phpexcel')->createWriter($preliquidacion, 'CSV')
                                                ->setDelimiter($caracterSeparacion)
                                                ->setUseBOM(true);
                // Crea el response
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                
                // Se añaden las cabeceras
                $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
                $response->headers->set('Content-Disposition', 'attachment; filename="'.$nombreArchivo.'"');
                $response->headers->set('Set-Cookie', 'fileDownload=true; path=/');
                
                return $response;
                break;
        }

        
    }

    public function generarPreliquidacionGenerica($fecha, $numeroResponsable) {

        // Crea la consulta
        $queryString = "SELECT

				concat(TO_CHAR(prestador.PROVNUMINT), concat('-', prestador.PROVNUMCUI)) AS \"CODIGO PRESTADOR\", 
				TRIM(prestador.provrazsoc) as \"PRESTADOR\",
				audita.nonocodigo as \"CODIGO PRESTACION\",
				TRIM(nomencla.NOMEDESBRE) as PRESTACION,
				audita.AUDACANPRE as CANTIDAD,
				audita.audamonaut as MONTO,
				audita.AUCANUMINT as \"NUMERO AUTORIZACION\",
				TRIM(aucabe.AUCANROAFI) as \"DOCUMENTO AFILIADO\",
				aucabe.AUCANOMAFI as \"NOMBRE AFILIADO\",
				TO_CHAR(aucabe.AUCAFECSOL, 'DD/MM/YYYY') as \"FECHA SOLICITUD\",
				preli.FMPRLNUMER as \"NRO PRELIQUIDACION\",
				preli.fmpepanio as ANO,
				preli.fmpepmes as MES,
				suc.pvsucodpos as \"CODIGO POSTAL\",
				loc.locadescri as LOCALIDAD

				FROM 

				US_IAPOS_PROD.audapra audita,
				us_iapos_prod.aucabez aucabe,
				US_IAPOS_PROD.PROVEEDO prestador, 
				us_iapos_prod.fmprliqu preli,
				us_iapos_prod.nomencla nomencla,
				us_iapos_prod.RESFACTU resp,
				US_IAPOS_PROD.pvsucur suc,
				us_iapos_prod.localida loc

				WHERE

				resp.RESFNUMINT = aucabe.AUCARESFNM and
				prestador.provnumint=suc.provnumint and
				suc.PVSUCODLOC=loc.locacodigo and
				preli.fmprlnumer = audita.AUDAPRELIQ and 
				audita.DELENUMDEL = aucabe.DELENUMDEL and
				audita.AUCANUMINT = aucabe.AUCANUMINT and
				aucabe.AUCANUMPRO = prestador.PROVNUMINT and 
				(nomencla.SECNCODIGO = audita.SECNCODIGO and 
				nomencla.SECANUMCAP = audita.SECANUMCAP and 
				nomencla.NOMENUMSUB = audita.NOMENUMSUB  and 
				nomencla.NOMENUMSEC = audita.NOMENUMSEC and 
				nomencla.SUBICODSUB = audita.SUBICODSUB and
				nomencla.ORINCODIGO = audita.ORINCODIGO) and 
				aucabe.AUCAANUFEC = TO_DATE('01/01/0001', 'dd/mm/yyyy') and
				preli.fmpepanio = " . $fecha->format("Y") . " and
				preli.fmpepmes = " . $fecha->format("n") . " and
				resp.RESFNUMINT = " . $numeroResponsable . "
				
				order by PRESTADOR";

        // Arma la cabecera genérica
        $cabecera = array("CODIGO PRESTADOR", "PRESTADOR", "CODIGO PRESTACION", "PRESTACION", "CANTIDAD", "MONTO", "NUMERO AUTORIZACION", "DOCUMENTO AFILIADO", "NOMBRE AFILIADO", "FECHA SOLICITUD", "ANO", "MES", "CODIGO POSTAL", "LOCALIDAD");

        return $this->generarArchivo($queryString, $cabecera);
                
    }

    public function generarPreliquidacionParticular($fecha, $entidad) {
        $queryString = "";
        $cabecera = "";

        switch ($entidad) {
            case 5:
                // Gremial Medica La Costa
                $numeroResponsable = 48203;

                $queryString = "	SELECT

					prestador.PROVNUMCUI as \"CUIT MEDICO\",
					TO_CHAR(aucabe.AUCAFECSOL, 'DD/MM/YYYY') as FECHA,
					TRIM(aucabe.AUCANOMAFI) as \"PACIENTE NOMBRE\",
					TRIM(REPLACE(prestador.provrazsoc, '- G COSTA')) as CLINICA,
					TRIM(aucabe.AUCANROAFI) as \"NRO AFILIADO\",
					audita.AUDACANPRE as CANTIDAD,
					audita.nonocodigo as \"CODIGO CIR\",
					TRIM(nomencla.NOMEDESBRE) as PRESTACION,
					audita.audamonaut as HONORARIOS,
					preli.FMPRLNUMER as \"NRO LIQUIDACION\"

					FROM

					US_IAPOS_PROD.audapra audita,
					us_iapos_prod.aucabez aucabe,
					US_IAPOS_PROD.PROVEEDO prestador, 
					us_iapos_prod.fmprliqu preli,
					us_iapos_prod.nomencla nomencla

					WHERE

					preli.fmprlnumer = audita.AUDAPRELIQ and 
					audita.DELENUMDEL = aucabe.DELENUMDEL and 
					audita.AUCANUMINT = aucabe.AUCANUMINT and 
					aucabe.AUCANUMPRO = prestador.PROVNUMINT and 
					(nomencla.SECNCODIGO = audita.SECNCODIGO and 
					nomencla.SECANUMCAP = audita.SECANUMCAP and 
					nomencla.NOMENUMSUB = audita.NOMENUMSUB  and 
					nomencla.NOMENUMSEC = audita.NOMENUMSEC and 
					nomencla.SUBICODSUB = audita.SUBICODSUB and 
					nomencla.ORINCODIGO = audita.ORINCODIGO ) and 
					aucabe.AUCAANUFEC = TO_DATE('01/01/0001', 'dd/mm/yyyy') and
					preli.fmpepanio = " . $fecha->format("Y") . " and
					preli.fmpepmes = " . $fecha->format("n") . " and
					aucabe.AUCARESFNM = " . $numeroResponsable . "

					ORDER BY CLINICA";
                $cabecera = array("CUIT MEDICO", "FECHA", "PACIENTE NOMBRE", "CLINICA", "NRO AFILIADO", "CANTIDAD", "CODIGO CIR", "PRESTACION", "HONORARIOS", "NRO LIQUIDACION");
                break;
            case 6:
                // Asociacion de Profesionales de la Sangre [Todas las coberturas]
                $numeroResponsable = 900246;

                // Crea la consulta
                $queryString = "SELECT

                        concat(TO_CHAR(prestador.PROVNUMINT), concat('-', prestador.PROVNUMCUI)) AS \"CODIGO PRESTADOR\", 
                        TRIM(prestador.provrazsoc) as \"PRESTADOR\",
                        cobert.gravdesgra as COBERTURA,
                        audita.nonocodigo as \"CODIGO PRESTACION\",
                        TRIM(nomencla.NOMEDESBRE) as PRESTACION,
                        audita.AUDACANPRE as CANTIDAD,
                        audita.audamonaut as MONTO,
                        audita.AUCANUMINT as \"NUMERO AUTORIZACION\",
                        TRIM(aucabe.AUCANROAFI) as \"DOCUMENTO AFILIADO\",
                        aucabe.AUCANOMAFI as \"NOMBRE AFILIADO\",
                        TO_CHAR(aucabe.AUCAFECSOL, 'DD/MM/YYYY') as \"FECHA SOLICITUD\",
                        preli.FMPRLNUMER as \"NRO PRELIQUIDACION\",
                        preli.fmpepanio as ANO,
                        preli.fmpepmes as MES,
                        suc.pvsucodpos as \"CODIGO POSTAL\",
                        loc.locadescri as LOCALIDAD

                        FROM 

                        US_IAPOS_PROD.audapra audita,
                        us_iapos_prod.aucabez aucabe,
                        US_IAPOS_PROD.PROVEEDO prestador, 
                        us_iapos_prod.fmprliqu preli,
                        us_iapos_prod.gravamen cobert,
                        us_iapos_prod.nomencla nomencla,
                        us_iapos_prod.RESFACTU resp,
                        US_IAPOS_PROD.pvsucur suc,
                        us_iapos_prod.localida loc

                        WHERE

                        resp.RESFNUMINT = aucabe.AUCARESFNM and
                        prestador.provnumint=suc.provnumint and
                        cobert.gravcodgra=aucabe.AuCaCodGra and
                        suc.PVSUCODLOC=loc.locacodigo and
                        preli.fmprlnumer = audita.AUDAPRELIQ and 
                        audita.DELENUMDEL = aucabe.DELENUMDEL and
                        audita.AUCANUMINT = aucabe.AUCANUMINT and
                        aucabe.AUCANUMPRO = prestador.PROVNUMINT and 
                        (nomencla.SECNCODIGO = audita.SECNCODIGO and 
                        nomencla.SECANUMCAP = audita.SECANUMCAP and 
                        nomencla.NOMENUMSUB = audita.NOMENUMSUB  and 
                        nomencla.NOMENUMSEC = audita.NOMENUMSEC and 
                        nomencla.SUBICODSUB = audita.SUBICODSUB and
                        nomencla.ORINCODIGO = audita.ORINCODIGO) and 
                        aucabe.AUCAANUFEC = TO_DATE('01/01/0001', 'dd/mm/yyyy') and
                        preli.fmpepanio = " . $fecha->format("Y") . " and
                        preli.fmpepmes = " . $fecha->format("n") . " and
                        resp.RESFNUMINT = " . $numeroResponsable . "
                        
                        order by PRESTADOR";

                // Arma la cabecera genérica
                $cabecera = array("CODIGO PRESTADOR", "PRESTADOR", "COBERTURA", "CODIGO PRESTACION", "PRESTACION", "CANTIDAD", "MONTO", "NUMERO AUTORIZACION", "DOCUMENTO AFILIADO", "NOMBRE AFILIADO", "FECHA SOLICITUD", "ANO", "MES", "CODIGO POSTAL", "LOCALIDAD");

                break;
            case 7:
                // Asociacion de Profesionales de la Sangre [Solo HEMOTERAPIA]
                $numeroResponsable = 900246;

                // Crea la consulta
                $queryString = "SELECT

                        concat(TO_CHAR(prestador.PROVNUMINT), concat('-', prestador.PROVNUMCUI)) AS \"CODIGO PRESTADOR\", 
                        TRIM(prestador.provrazsoc) as \"PRESTADOR\",
                        cobert.gravdesgra as COBERTURA,
                        audita.nonocodigo as \"CODIGO PRESTACION\",
                        TRIM(nomencla.NOMEDESBRE) as PRESTACION,
                        audita.AUDACANPRE as CANTIDAD,
                        audita.audamonaut as MONTO,
                        audita.AUCANUMINT as \"NUMERO AUTORIZACION\",
                        TRIM(aucabe.AUCANROAFI) as \"DOCUMENTO AFILIADO\",
                        aucabe.AUCANOMAFI as \"NOMBRE AFILIADO\",
                        TO_CHAR(aucabe.AUCAFECSOL, 'DD/MM/YYYY') as \"FECHA SOLICITUD\",
                        preli.FMPRLNUMER as \"NRO PRELIQUIDACION\",
                        preli.fmpepanio as ANO,
                        preli.fmpepmes as MES,
                        suc.pvsucodpos as \"CODIGO POSTAL\",
                        loc.locadescri as LOCALIDAD

                        FROM 

                        US_IAPOS_PROD.audapra audita,
                        us_iapos_prod.aucabez aucabe,
                        US_IAPOS_PROD.PROVEEDO prestador, 
                        us_iapos_prod.fmprliqu preli,
                        us_iapos_prod.gravamen cobert,
                        us_iapos_prod.nomencla nomencla,
                        us_iapos_prod.RESFACTU resp,
                        US_IAPOS_PROD.pvsucur suc,
                        us_iapos_prod.localida loc

                        WHERE

                        resp.RESFNUMINT = aucabe.AUCARESFNM and
                        prestador.provnumint=suc.provnumint and
                        cobert.gravcodgra=aucabe.AuCaCodGra and
                        suc.PVSUCODLOC=loc.locacodigo and
                        preli.fmprlnumer = audita.AUDAPRELIQ and 
                        audita.DELENUMDEL = aucabe.DELENUMDEL and
                        audita.AUCANUMINT = aucabe.AUCANUMINT and
                        aucabe.AUCANUMPRO = prestador.PROVNUMINT and 
                        (nomencla.SECNCODIGO = audita.SECNCODIGO and 
                        nomencla.SECANUMCAP = audita.SECANUMCAP and 
                        nomencla.NOMENUMSUB = audita.NOMENUMSUB  and 
                        nomencla.NOMENUMSEC = audita.NOMENUMSEC and 
                        nomencla.SUBICODSUB = audita.SUBICODSUB and
                        nomencla.ORINCODIGO = audita.ORINCODIGO) and 
                        cobert.gravdesgra = 'HEMOTERAPIA' and
                        aucabe.AUCAANUFEC = TO_DATE('01/01/0001', 'dd/mm/yyyy') and
                        preli.fmpepanio = " . $fecha->format("Y") . " and
                        preli.fmpepmes = " . $fecha->format("n") . " and
                        resp.RESFNUMINT = " . $numeroResponsable . "
                        
                        order by PRESTADOR";

                // Arma la cabecera genérica
                $cabecera = array("CODIGO PRESTADOR", "PRESTADOR", "COBERTURA", "CODIGO PRESTACION", "PRESTACION", "CANTIDAD", "MONTO", "NUMERO AUTORIZACION", "DOCUMENTO AFILIADO", "NOMBRE AFILIADO", "FECHA SOLICITUD", "ANO", "MES", "CODIGO POSTAL", "LOCALIDAD");

                break;
        }

        return $this->generarArchivo($queryString, $cabecera);
                
    }

    public function generarArchivo($queryString, $cabecera) {
        // Obtiene el entity manager
        $em = $this->getDoctrine()->getManager();

        // Ejecuta la consulta y asigna iterador
        $iterador = $em->getConnection()->query($queryString);
        
        // Solicita el servicio PHP EXCEL y crea un objeto excel
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        // Crea las propiedades
        $phpExcelObject->getProperties()
            ->setCreator("Sectorial Informatica IAPOS")
            ->setLastModifiedBy("Sectorial Informatica IAPOS")
            ->setTitle("Preliquidacion")
            ->setSubject("Preliquidacion")
            ->setDescription("Preliquidacion exportada desde sistema");

        // Establece como hoja activa la primera, y le asigna un título
        $phpExcelObject->setActiveSheetIndex(0);
        $hojaActiva = $phpExcelObject->getActiveSheet();
        $hojaActiva->setTitle('Preliquidacion');
        
        // Agrega los campos de la cabecera
        foreach ($cabecera as $indice => $valor) {
            $hojaActiva->setCellValueByColumnAndRow($indice, 1, $valor);
        }
        
        $filaActual = 2;
        while (is_object($iterador) AND ( $array = $iterador->fetch()) !== FALSE) {
            // Agrega cada campo de la fila correspondiente
            foreach ($cabecera as $indice => $valor) {
                $hojaActiva->setCellValueByColumnAndRow($indice, $filaActual, $array[$valor]);
            }
            $filaActual++;            
        }
        
        return $phpExcelObject;
        
    }

}
