<?php

namespace IAPOS\GeneralBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('IAPOSGeneralBundle:Default:index.html.twig');
     
    }
}
