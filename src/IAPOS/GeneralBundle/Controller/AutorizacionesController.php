<?php

namespace IAPOS\GeneralBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AutorizacionesController extends Controller
{
    public function detalleIndexAction()
    {
        return $this->render('IAPOSGeneralBundle:Autorizaciones:detalleIndex.html.twig');     
    }

    public function detalLeAction($numeroAutorizacion)
    {
    	// Obtiene el entity manager
        $em = $this->getDoctrine()->getManager();

        // Busca la autorizacion indicada
        $autorizacion = $em->getRepository('IAPOSGeneralBundle:AutorizacionCabecera')->findOneByNumeroAutorizacion($numeroAutorizacion);
        
        return $this->render('IAPOSGeneralBundle:Autorizaciones:detalle.html.twig', array('autorizacion'=>$autorizacion, 'numeroAutorizacion' => $numeroAutorizacion));
     
    }
}
