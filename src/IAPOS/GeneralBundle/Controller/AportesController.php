<?php

namespace IAPOS\GeneralBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AportesController extends Controller
{
    public function descuentosPersonaAction($documentoPersona, $fechaDesde, $fechaHasta)
    {
  		
        // Obtiene el entity manager
        $em = $this->getDoctrine()->getManager();

        $fechaDesde = new \DateTime($fechaDesde);
        $fechaHasta = new \DateTime($fechaHasta);
        $descuentosRH = '';
        $descuentosCJ = '';

        
        // Si la fecha desde es nula y la hasta no lo es
        if ( !$this->fechaEsNula($fechaDesde) && $this->fechaEsNula($fechaHasta) ){
            $fechaHasta = clone $fechaDesde;
            $fechaHasta->add(new \DateInterval('P4M'));   
        }
        // Si las dos fechas son nulas
        else if ( $this->fechaEsNula($fechaDesde) && !$this->fechaEsNula($fechaHasta) ){
            $fechaDesde = clone $fechaHasta;
            $fechaDesde->sub(new \DateInterval('P4M')); 
        }
        // Si ninguna de las fechas es nula
        else if ( $this->fechaEsNula($fechaDesde) && $this->fechaEsNula($fechaHasta) ){
            $fechaHasta = new \DateTime();
            $fechaDesde = clone $fechaHasta;
            $fechaDesde->sub(new \DateInterval('P4M'));
        }

        // Recupera todos los posibles descuentos existentes para la persona
        $descuentosRH = $em->getRepository('IAPOSGeneralBundle:DescuentoPersona')->findDescuentosEnRango($documentoPersona, $fechaDesde, $fechaHasta);
        $descuentosCJ = $em->getRepository('IAPOSGeneralBundle:DescuentoPersonaCJ')->findDescuentosEnRango($documentoPersona, $fechaDesde, $fechaHasta);

        return $this->render('IAPOSGeneralBundle:Aportes:descuentosPorPersona.html.twig', array('descuentosPersonaRH'=> $descuentosRH, 'descuentosPersonaCJ'=> $descuentosCJ, 'documentoPersona' => $documentoPersona, 'fechaDesde' => $fechaDesde, 'fechaHasta' => $fechaHasta));
    }

    public function indexAction()
    {
        
        return $this->render('IAPOSGeneralBundle:Aportes:descuentos.html.twig');
    }

    /* Indica mediante true o false si una fecha es nula */
    public function fechaEsNula($fecha)
    {
        if(0 == strcmp($fecha->format("Y"), '-0001'))
            return true;
        return false;
    }


}