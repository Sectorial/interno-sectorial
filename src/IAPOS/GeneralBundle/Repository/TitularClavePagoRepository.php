<?php

namespace IAPOS\GeneralBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * TitularClavePagoRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TitularClavePagoRepository extends EntityRepository
{
}
