<?php

namespace IAPOS\GeneralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TitularClavePago
 */
class TitularClavePago
{
    /**
     * @var int
     */
    private $numeroTitular;

    /**
     * @var int
     */
    private $codigoOrganizacion;

    /**
     * @var string
     */
    private $codigoClavePago;

    /**
     * @var \DateTime
     */
    private $fechaBaja;

    /**
     * @var string
     */
    private $esPrincipal;

    /**
     * @var ClavePago
     */
    private $clavePago;


    /**
     * @var Titular
     */
    private $titular;

    /**
     * Set numeroTitular
     *
     * @param integer $numeroTitular
     * @return TitularClavePago
     */
    public function setNumeroTitular($numeroTitular)
    {
        $this->numeroTitular = $numeroTitular;

        return $this;
    }

    /**
     * Get numeroTitular
     *
     * @return integer 
     */
    public function getNumeroTitular()
    {
        return $this->numeroTitular;
    }

    /**
     * Set codigoOrganizacion
     *
     * @param integer $codigoOrganizacion
     * @return TitularClavePago
     */
    public function setCodigoOrganizacion($codigoOrganizacion)
    {
        $this->codigoOrganizacion = $codigoOrganizacion;

        return $this;
    }

    /**
     * Get codigoOrganizacion
     *
     * @return integer 
     */
    public function getCodigoOrganizacion()
    {
        return $this->codigoOrganizacion;
    }

    /**
     * Set codigoClavePago
     *
     * @param string $codigoClavePago
     * @return TitularClavePago
     */
    public function setCodigoClavePago($codigoClavePago)
    {
        $this->codigoClavePago = $codigoClavePago;

        return $this;
    }

    /**
     * Get codigoClavePago
     *
     * @return string 
     */
    public function getCodigoClavePago()
    {
        return $this->codigoClavePago;
    }

    /**
     * Set fechaBaja
     *
     * @param \DateTime $fechaBaja
     * @return TitularClavePago
     */
    public function setFechaBaja($fechaBaja)
    {
        $this->fechaBaja = $fechaBaja;

        return $this;
    }

    /**
     * Get fechaBaja
     *
     * @return \DateTime 
     */
    public function getFechaBaja()
    {
        return $this->fechaBaja;
    }

    /**
     * Set esPrincipal
     *
     * @param string $esPrincipal
     * @return TitularClavePago
     */
    public function setEsPrincipal($esPrincipal)
    {
        $this->esPrincipal = $esPrincipal;

        return $this;
    }

    /**
     * Get esPrincipal
     *
     * @return string 
     */
    public function getEsPrincipal()
    {
        return $this->esPrincipal;
    }


    /**
     * Set clavePago
     *
     * @param ClavePago $clavePago
     * @return TitularClavePago
     */
    public function setClavePago($clavePago)
    {
        $this->clavePago = $clavePago;

        return $this;
    }

    /**
     * Get clavePago
     *
     * @return ClavePago 
     */
    public function getClavePago()
    {
        return $this->clavePago;
    }

    /**
     * Set titular
     *
     * @param Titular $titular
     * @return TitularClavePago
     */
    public function setTitular($titular)
    {
        $this->titular = $titular;

        return $this;
    }

    /**
     * Get titular
     *
     * @return Titular 
     */
    public function getTitular()
    {
        return $this->titular;
    }
}
