<?php

namespace IAPOS\GeneralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DescuentoPersonaCJ
 */
class DescuentoPersonaCJ
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $anio;

    /**
     * @var int
     */
    private $mes;

    /**
     * @var string
     */
    private $bICJCTIFCO;

    /**
     * @var int
     */
    private $bISISCODIG;

    /**
     * @var Descuento
     */
    private $descuento;


    /**
     * @var string
     */
    private $bITILCODIG; 

    /**
     * @var string
     */
    private $tipoBeneficio;

    /**
     * @var string
     */
    private $origen;

    /**
     * @var int
     */
    private $bICJDSECUE;

    /**
     * @var float
     */
    private $porcentaje;

    /**
     * @var float
     */
    private $importe;

    /**
     * @var string
     */
    private $apellidoNombre;

    /**
     * @var string
     */
    private $documentoPersona;


    /**
     * Set descuento
     *
     * @param Descuento $descuento
     * @return DescuentoPersona
     */
   public function setDescuento($descuento)
   {
        $this->descuento = $descuento;

        return $this;
   }

   /**
     * Get descuento
     *
     * @return Descuento 
     */
   public function getDescuento()
   {
        return $this->descuento;
   }

    /**
     * Set anio
     *
     * @param integer $anio
     * @return DescuentoPersonaCJ
     */
    public function setAnio($anio)
    {
        $this->anio = $anio;

        return $this;
    }

    /**
     * Get anio
     *
     * @return integer 
     */
    public function getAnio()
    {
        return $this->anio;
    }

    /**
     * Set mes
     *
     * @param integer $mes
     * @return DescuentoPersonaCJ
     */
    public function setMes($mes)
    {
        $this->mes = $mes;

        return $this;
    }

    /**
     * Get mes
     *
     * @return integer 
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * Set bICJCTIFCO
     *
     * @param string $bICJCTIFCO
     * @return DescuentoPersonaCJ
     */
    public function setBICJCTIFCO($bICJCTIFCO)
    {
        $this->bICJCTIFCO = $bICJCTIFCO;

        return $this;
    }

    /**
     * Get bICJCTIFCO
     *
     * @return string 
     */
    public function getBICJCTIFCO()
    {
        return $this->bICJCTIFCO;
    }

    /**
     * Set tipoBeneficio
     *
     * @param string $tipoBeneficio
     * @return string
     */
    public function setTipoBeneficio($tipoBeneficio)
    {
        $this->tipoBeneficio = $tipoBeneficio;

        return $this;
    }

    /**
     * Get tipoBeneficio
     *
     * @return string 
     */
    public function getTipoBeneficio()
    {
        return $this->tipoBeneficio;
    }

    /**
     * Set bISISCODIG
     *
     * @param integer $bISISCODIG
     * @return DescuentoPersonaCJ
     */
    public function setBISISCODIG($bISISCODIG)
    {
        $this->bISISCODIG = $bISISCODIG;

        return $this;
    }

    /**
     * Get bISISCODIG
     *
     * @return integer 
     */
    public function getBISISCODIG()
    {
        return $this->bISISCODIG;
    }

    /**
     * Set bITILCODIG
     *
     * @param string $bITILCODIG
     * @return DescuentoPersonaCJ
     */
    public function setBITILCODIG($bITILCODIG)
    {
        $this->bITILCODIG = $bITILCODIG;

        return $this;
    }

    /**
     * Get bITILCODIG
     *
     * @return string 
     */
    public function getBITILCODIG()
    {
        return $this->bITILCODIG;
    }

    /**
     * Set origen
     *
     * @param string $origen
     * @return DescuentoPersonaCJ
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * Get origen
     *
     * @return string 
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * Set bICJDSECUE
     *
     * @param integer $bICJDSECUE
     * @return DescuentoPersonaCJ
     */
    public function setBICJDSECUE($bICJDSECUE)
    {
        $this->bICJDSECUE = $bICJDSECUE;

        return $this;
    }

    /**
     * Get bICJDSECUE
     *
     * @return integer 
     */
    public function getBICJDSECUE()
    {
        return $this->bICJDSECUE;
    }

    /**
     * Set porcentaje
     *
     * @param float $porcentaje
     * @return DescuentoPersonaCJ
     */
    public function setPorcentaje($porcentaje)
    {
        $this->porcentaje = $porcentaje;

        return $this;
    }

    /**
     * Get porcentaje
     *
     * @return float 
     */
    public function getPorcentaje()
    {
        return $this->porcentaje;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return DescuentoPersonaCJ
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Set apellidoNombre
     *
     * @param string $apellidoNombre
     * @return DescuentoPersonaCJ
     */
    public function setApellidoNombre($apellidoNombre)
    {
        $this->apellidoNombre = $apellidoNombre;

        return $this;
    }

    /**
     * Get apellidoNombre
     *
     * @return string 
     */
    public function getApellidoNombre()
    {
        return $this->apellidoNombre;
    }

    /**
     * Set documentoPersona
     *
     * @param string $documentoPersona
     * @return DescuentoPersonaCJ
     */
    public function setDocumentoPersona($documentoPersona)
    {
        $this->documentoPersona = $documentoPersona;

        return $this;
    }

    /**
     * Get documentoPersona
     *
     * @return string 
     */
    public function getDocumentoPersona()
    {
        return $this->documentoPersona;
    }
}
