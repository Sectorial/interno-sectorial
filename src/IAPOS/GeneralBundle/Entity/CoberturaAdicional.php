<?php

namespace IAPOS\GeneralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CoberturaAdicional
 */
class CoberturaAdicional
{
   
    /**
     * @var int
     */
    private $numeroTitular;

    /**
     * @var int
     */
    private $numeroMiembro;

    /**
     * @var int
     */
    private $secuencia;

    /**
     * @var int
     */
    private $codigoCobertura;

    /**
     * @var \DateTime
     */
    private $fechaVigencia;

    /**
     * @var \DateTime
     */
    private $fechaBaja;

    /**
     * @var \DateTime
     */
    private $fechaAnulacion;

    /**
     * @var \DateTime
     */
    private $fechaCobertura;

    /**
     * @var Miembro
     */
    private $miembro;

    /**
     * Set numeroTitular
     *
     * @param integer $numeroTitular
     * @return CoberturaAdicional
     */
    public function setNumeroTitular($numeroTitular)
    {
        $this->numeroTitular = $numeroTitular;

        return $this;
    }

    /**
     * Get numeroTitular
     *
     * @return integer 
     */
    public function getNumeroTitular()
    {
        return $this->numeroTitular;
    }

    /**
     * Set numeroMiembro
     *
     * @param integer $numeroMiembro
     * @return CoberturaAdicional
     */
    public function setNumeroMiembro($numeroMiembro)
    {
        $this->numeroMiembro = $numeroMiembro;

        return $this;
    }

    /**
     * Get numeroMiembro
     *
     * @return integer 
     */
    public function getNumeroMiembro()
    {
        return $this->numeroMiembro;
    }

    /**
     * Set secuencia
     *
     * @param integer $secuencia
     * @return CoberturaAdicional
     */
    public function setSecuencia($secuencia)
    {
        $this->secuencia = $secuencia;

        return $this;
    }

    /**
     * Get secuencia
     *
     * @return integer 
     */
    public function getSecuencia()
    {
        return $this->secuencia;
    }

    /**
     * Set codigoCobertura
     *
     * @param integer $codigoCobertura
     * @return CoberturaAdicional
     */
    public function setCodigoCobertura($codigoCobertura)
    {
        $this->codigoCobertura = $codigoCobertura;

        return $this;
    }

    /**
     * Get codigoCobertura
     *
     * @return integer 
     */
    public function getCodigoCobertura()
    {
        return $this->codigoCobertura;
    }

    /**
     * Set fechaVigencia
     *
     * @param \DateTime $fechaVigencia
     * @return CoberturaAdicional
     */
    public function setFechaVigencia($fechaVigencia)
    {
        $this->fechaVigencia = $fechaVigencia;

        return $this;
    }

    /**
     * Get fechaVigencia
     *
     * @return \DateTime 
     */
    public function getFechaVigencia()
    {
        return $this->fechaVigencia;
    }

    /**
     * Set fechaBaja
     *
     * @param \DateTime $fechaBaja
     * @return CoberturaAdicional
     */
    public function setFechaBaja($fechaBaja)
    {
        $this->fechaBaja = $fechaBaja;

        return $this;
    }

    /**
     * Get fechaBaja
     *
     * @return \DateTime 
     */
    public function getFechaBaja()
    {
        return $this->fechaBaja;
    }

    /**
     * Set fechaCobertura
     *
     * @param \DateTime $fechaCobertura
     * @return CoberturaAdicional
     */
    public function setFechaCobertura($fechaCobertura)
    {
        $this->fechaCobertura = $fechaCobertura;

        return $this;
    }

    /**
     * Get fechaCobertura
     *
     * @return \DateTime 
     */
    public function getFechaCobertura()
    {
        return $this->fechaCobertura;
    }

    /**
     * Set fechaAnulacion
     *
     * @param \DateTime $fechaAnulacion
     * @return CoberturaAdicional
     */
    public function setFechaAnulacion($fechaAnulacion)
    {
        $this->fechaAnulacion = $fechaAnulacion;

        return $this;
    }

    /**
     * Get fechaAnulacion
     *
     * @return \DateTime 
     */
    public function getFechaAnulacion()
    {
        return $this->fechaAnulacion;
    }

    /**
     * Set miembro
     *
     * @param Miembro $miembro
     * @return CoberturaAdicional
     */
    public function setMiembra($miembro)
    {
        $this->miembro = $miembro;

        return $this;
    }

    /**
     * Get miembro
     *
     * @return Miembro 
     */
    public function getMiembro()
    {
        return $this->miembro;
    }
}
