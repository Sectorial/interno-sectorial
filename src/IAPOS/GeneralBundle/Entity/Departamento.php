<?php

namespace IAPOS\GeneralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Departamento
 */
class Departamento
{
    
    /**
     * @var string
     */
    private $codigoProvincia;

    /**
     * @var int
     */
    private $codigoDepartamento;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * @var Provincia
     */
    private $provincia;

    /**
     * @var 
     */
    private $localidades;




    /**
     * Set codigoProvincia
     *
     * @param string $codigoProvincia
     * @return Departamento
     */
    public function setCodigoProvincia($codigoProvincia)
    {
        $this->codigoProvincia = $codigoProvincia;

        return $this;
    }

    /**
     * Get codigoProvincia
     *
     * @return string 
     */
    public function getCodigoProvincia()
    {
        return $this->codigoProvincia;
    }

    /**
     * Set codigoDepartamento
     *
     * @param integer $codigoDepartamento
     * @return Departamento
     */
    public function setCodigoDepartamento($codigoDepartamento)
    {
        $this->codigoDepartamento = $codigoDepartamento;

        return $this;
    }

    /**
     * Get codigoDepartamento
     *
     * @return integer 
     */
    public function getCodigoDepartamento()
    {
        return $this->codigoDepartamento;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Departamento
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set provincia
     *
     * @param Provincia $provincia
     * @return Departamento
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return Provincia 
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set localidades
     *
     * @param  $localidades
     * @return Departamento
     */
    public function setLocalidades($localidades)
    {
        $this->localidades = $localidades;

        return $this;
    }

    /**
     * Get localidades
     *
     * @return 
     */
    public function getLocalidades()
    {
        return $this->localidades;
    }
}
