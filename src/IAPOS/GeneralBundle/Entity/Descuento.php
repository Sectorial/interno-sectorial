<?php

namespace IAPOS\GeneralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Descuento
 */
class Descuento
{
   
    /**
     * @var int
     */
    private $codigoSistema;

    /**
     * @var string
     */
    private $codigoConcepto;

    /**
     * @var string
     */
    private $descripcion;

    /**
     * Set codigoSistema
     *
     * @param integer $codigoSistema
     * @return Descuento
     */
    public function setCodigoSistema($codigoSistema)
    {
        $this->codigoSistema = $codigoSistema;

        return $this;
    }

    /**
     * Get codigoSistema
     *
     * @return integer 
     */
    public function getCodigoSistema()
    {
        return $this->codigoSistema;
    }

    /**
     * Set codigoConcepto
     *
     * @param string $codigoConcepto
     * @return Descuento
     */
    public function setCodigoConcepto($codigoConcepto)
    {
        $this->codigoConcepto = $codigoConcepto;

        return $this;
    }

    /**
     * Get codigoConcepto
     *
     * @return string 
     */
    public function getCodigoConcepto()
    {
        return $this->codigoConcepto;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Descuento
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}
