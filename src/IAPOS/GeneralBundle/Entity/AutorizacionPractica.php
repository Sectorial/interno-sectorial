<?php

namespace IAPOS\GeneralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AutorizacionPractica
 */
class AutorizacionPractica
{

    /**
     * @var int
     */
    private $numeroDelegacion;

    /**
     * @var int
     */
    private $numeroAutorizacion;

    /**
     * @var string
     */
    private $numeroPracticaAutorizacion;

    /**
     * @var string
     */
    private $codigoPrestacion;

    /**
     * @var float
     */
    private $cantidadAutorizada;

    /**
     * @var float
     */
    private $cantidadSolicitada;

    /**
     * @var string
     */
    private $marcaAutorizado;

    /**
     * @var string
     */
    private $marcaAuditado;

    /**
     * @var \DateTime
     */
    private $fechaAutorizado;

    /**
     * @var \DateTime
     */
    private $fechaAuditado;

    /**
     * @var string
     */
    private $horaAutorizado;

    /**
     * @var string
     */
    private $horaAuditado;

    /**
     * @var string
     */
    private $montoAutorizado;

    /**
     * @var float
     */
    private $cantidadConsumida;

    /**
     * @var int
     */
    private $numeroPreliquidacion;

    /**
     * @var string
     */
    private $usuarioAlta;

    /**
     * @var \DateTime
     */
    private $fechaHoraAlta;

    /**
     * @var string
     */
    private $usuarioAnulacion;

    /**
     * @var \DateTime
     */
    private $fechaAnulacion;

    /**
     * @var Cobertura
     */
    private $cobertura;

    /**
     * @var AutorizacionCabecera
     */
    private $autorizacion;

    /**
     * @var PracticaNomenclador
     */
    private $practicaNomenclador;

    /**
     * @var Array HistorialAutorizacionPractica
     */
    private $modificaciones;




    /**
     * Set numeroDelegacion
     *
     * @param integer $numeroDelegacion
     * @return AutorizacionPractica
     */
    public function setNumeroDelegacion($numeroDelegacion)
    {
        $this->numeroDelegacion = $numeroDelegacion;

        return $this;
    }

    /**
     * Get numeroDelegacion
     *
     * @return integer 
     */
    public function getNumeroDelegacion()
    {
        return $this->numeroDelegacion;
    }

    /**
     * Set numeroAutorizacion
     *
     * @param integer $numeroAutorizacion
     * @return AutorizacionPractica
     */
    public function setNumeroAutorizacion($numeroAutorizacion)
    {
        $this->numeroAutorizacion = $numeroAutorizacion;

        return $this;
    }

    /**
     * Get numeroAutorizacion
     *
     * @return integer 
     */
    public function getNumeroAutorizacion()
    {
        return $this->numeroAutorizacion;
    }

    /**
     * Set numeroPracticaAutorizacion
     *
     * @param string $numeroPracticaAutorizacion
     * @return AutorizacionPractica
     */
    public function setNumeroPracticaAutorizacion($numeroPracticaAutorizacion)
    {
        $this->numeroPracticaAutorizacion = $numeroPracticaAutorizacion;

        return $this;
    }

    /**
     * Get numeroPracticaAutorizacion
     *
     * @return string 
     */
    public function getNumeroPracticaAutorizacion()
    {
        return $this->numeroPracticaAutorizacion;
    }

    /**
     * Set codigoPrestacion
     *
     * @param string $codigoPrestacion
     * @return AutorizacionPractica
     */
    public function setCodigoPrestacion($codigoPrestacion)
    {
        $this->codigoPrestacion = $codigoPrestacion;

        return $this;
    }

    /**
     * Get codigoPrestacion
     *
     * @return string 
     */
    public function getCodigoPrestacion()
    {
        return $this->codigoPrestacion;
    }

    /**
     * Set cantidadAutorizada
     *
     * @param float $cantidadAutorizada
     * @return AutorizacionPractica
     */
    public function setCantidadAutorizada($cantidadAutorizada)
    {
        $this->cantidadAutorizada = $cantidadAutorizada;

        return $this;
    }

    /**
     * Get cantidadAutorizada
     *
     * @return float 
     */
    public function getCantidadAutorizada()
    {
        return $this->cantidadAutorizada;
    }

    /**
     * Set marcaAutorizado
     *
     * @param string $marcaAutorizado
     * @return AutorizacionPractica
     */
    public function setMarcaAutorizado($marcaAutorizado)
    {
        $this->marcaAutorizado = $marcaAutorizado;

        return $this;
    }

    /**
     * Get marcaAutorizado
     *
     * @return string 
     */
    public function getMarcaAutorizado()
    {
        return $this->marcaAutorizado;
    }

    /**
     * Set marcaAuditado
     *
     * @param string $marcaAuditado
     * @return AutorizacionPractica
     */
    public function setMarcaAuditado($marcaAuditado)
    {
        $this->marcaAuditado = $marcaAuditado;

        return $this;
    }

    /**
     * Get marcaAuditado
     *
     * @return string 
     */
    public function getMarcaAuditado()
    {
        return $this->marcaAuditado;
    }

    /**
     * Set fechaAutorizado
     *
     * @param \DateTime $fechaAutorizado
     * @return AutorizacionPractica
     */
    public function setFechaAutorizado($fechaAutorizado)
    {
        $this->fechaAutorizado = $fechaAutorizado;

        return $this;
    }

    /**
     * Get fechaAutorizado
     *
     * @return \DateTime 
     */
    public function getFechaAutorizado()
    {
        return $this->fechaAutorizado;
    }

    /**
     * Set fechaAuditado
     *
     * @param \DateTime $fechaAuditado
     * @return AutorizacionPractica
     */
    public function setFechaAuditado($fechaAuditado)
    {
        $this->fechaAuditado = $fechaAuditado;

        return $this;
    }

    /**
     * Get fechaAuditado
     *
     * @return \DateTime 
     */
    public function getFechaAuditado()
    {
        return $this->fechaAuditado;
    }

    /**
     * Set horaAutorizado
     *
     * @param string $horaAutorizado
     * @return AutorizacionPractica
     */
    public function setHoraAutorizado($horaAutorizado)
    {
        $this->horaAutorizado = $horaAutorizado;

        return $this;
    }

    /**
     * Get horaAutorizado
     *
     * @return string 
     */
    public function getHoraAutorizado()
    {
        return $this->horaAutorizado;
    }

    /**
     * Set horaAuditado
     *
     * @param string $horaAuditado
     * @return AutorizacionPractica
     */
    public function setHoraAuditado($horaAuditado)
    {
        $this->horaAuditado = $horaAuditado;

        return $this;
    }

    /**
     * Get horaAuditado
     *
     * @return string 
     */
    public function getHoraAuditado()
    {
        return $this->horaAuditado;
    }

    /**
     * Set montoAutorizado
     *
     * @param string $montoAutorizado
     * @return AutorizacionPractica
     */
    public function setMontoAutorizado($montoAutorizado)
    {
        $this->montoAutorizado = $montoAutorizado;

        return $this;
    }

    /**
     * Get montoAutorizado
     *
     * @return string 
     */
    public function getMontoAutorizado()
    {
        return $this->montoAutorizado;
    }

    /**
     * Set cantidadConsumida
     *
     * @param float $cantidadConsumida
     * @return AutorizacionPractica
     */
    public function setCantidadConsumida($cantidadConsumida)
    {
        $this->cantidadConsumida = $cantidadConsumida;

        return $this;
    }

    /**
     * Get cantidadConsumida
     *
     * @return float 
     */
    public function getCantidadConsumida()
    {
        return $this->cantidadConsumida;
    }

    /**
     * Set numeroPreliquidacion
     *
     * @param integer $numeroPreliquidacion
     * @return AutorizacionPractica
     */
    public function setNumeroPreliquidacion($numeroPreliquidacion)
    {
        $this->numeroPreliquidacion = $numeroPreliquidacion;

        return $this;
    }

    /**
     * Get numeroPreliquidacion
     *
     * @return integer 
     */
    public function getNumeroPreliquidacion()
    {
        return $this->numeroPreliquidacion;
    }

    /**
     * Set usuarioAlta
     *
     * @param string $usuarioAlta
     * @return AutorizacionPractica
     */
    public function setUsuarioAlta($usuarioAlta)
    {
        $this->usuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get usuarioAlta
     *
     * @return string 
     */
    public function getUsuarioAlta()
    {
        return $this->usuarioAlta;
    }

    /**
     * Set fechaHoraAlta
     *
     * @param \DateTime $fechaHoraAlta
     * @return AutorizacionPractica
     */
    public function setFechaHoraAlta($fechaHoraAlta)
    {
        $this->fechaHoraAlta = $fechaHoraAlta;

        return $this;
    }

    /**
     * Get fechaHoraAlta
     *
     * @return \DateTime 
     */
    public function getFechaHoraAlta()
    {
        return $this->fechaHoraAlta;
    }

    /**
     * Set usuarioAnulacion
     *
     * @param string $usuarioAnulacion
     * @return AutorizacionPractica
     */
    public function setUsuarioAnulacion($usuarioAnulacion)
    {
        $this->usuarioAnulacion = $usuarioAnulacion;

        return $this;
    }

    /**
     * Get usuarioAnulacion
     *
     * @return string 
     */
    public function getUsuarioAnulacion()
    {
        return $this->usuarioAnulacion;
    }

    /**
     * Set fechaAnulacion
     *
     * @param \DateTime $fechaAnulacion
     * @return AutorizacionPractica
     */
    public function setFechaAnulacion($fechaAnulacion)
    {
        $this->fechaAnulacion = $fechaAnulacion;

        return $this;
    }

    /**
     * Get fechaAnulacion
     *
     * @return \DateTime 
     */
    public function getFechaAnulacion()
    {
        return $this->fechaAnulacion;
    }


     /**
     * Set cobertura
     *
     * @param Cobertura $cobertura
     * @return AutorizacionPractica
     */
    public function setCobertura($cobertura)
    {
        $this->cobertura = $cobertura;

        return $this;
    }

    /**
     * Get cobertura
     *
     * @return Cobertura 
     */
    public function getCobertura()
    {
        return $this->cobertura;
    }


    /**
     * Set autorizacion
     *
     * @param AutorizacionCabecera $autorizacion
     * @return AutorizacionPractica
     */
    public function setAutorizacion($autorizacion)
    {
        $this->autorizacion = $autorizacion;

        return $this;
    }

    /**
     * Get autorizacion
     *
     * @return AutorizacionCabecera 
     */
    public function getAutorizacion()
    {
        return $this->autorizacion;
    }

    /**
     * Set practicaNomenclador
     *
     * @param PracticaNomenclador $practicaNomenclador
     * @return AutorizacionPractica
     */
    public function setPracticaNomenclador($practicaNomenclador)
    {
        $this->practicaNomenclador = $practicaNomenclador;

        return $this;
    }

    /**
     * Get practicaNomenclador
     *
     * @return PracticaNomenclador 
     */
    public function getPracticaNomenclador()
    {
        return $this->practicaNomenclador;
    }

    /**
     * Set cantidadSolicitada
     *
     * @param float $cantidadSolicitada
     * @return AutorizacionPractica
     */
    public function setCantidadSolicitada($cantidadSolicitada)
    {
        $this->cantidadSolicitada = $cantidadSolicitada;

        return $this;
    }

    /**
     * Get cantidadSolicitada
     *
     * @return float 
     */
    public function getCantidadSolicitada()
    {
        return $this->cantidadSolicitada;
    }

    public function getEstaAnulada(){
        if(0 == strcmp($this->getFechaAnulacion()->format("Y"), '0001'))
            return false;
        return true;
    }

    // Devuelve el estado de la práctica
    public function getEstado(){
        if(0 == strcmp($this->getMarcaAuditado(),"DIF") && 0 == strcmp($this->getMarcaAutorizado(),"DIF"))
            return "DIFERIDO MED y ADM";
        elseif(0 == strcmp($this->getMarcaAuditado(),"DIF"))
            return "DIFERIDO MED";
        elseif(0 == strcmp($this->getMarcaAutorizado(),"DIF"))
            return "DIFERIDO ADM";
        else
            return "AUTORIZADO";  
    }

    /**
     * Set modificaciones
     *
     * @param Array HistorialAutorizacionPractica $modificaciones
     * @return AutorizacionPractica
     */
    public function setModificaciones($modificaciones)
    {
        $this->modificaciones = $modificaciones;

        return $this;
    }

    /**
     * Get modificaciones
     *
     * @return Array HistorialAutorizacionPractica 
     */
    public function getModificaciones()
    {
        return $this->modificaciones;
    }
}
