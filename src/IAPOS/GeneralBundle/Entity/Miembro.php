<?php

namespace IAPOS\GeneralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Miembro
 */
class Miembro
{
        /**
     * @var int
     */
    private $numeroTitular;

    /**
     * @var int
     */
    private $codigoOrganizacion;

    /**
     * @var int
     */
    private $numeroMiembro;

    /**
     * @var string
     */
    private $apellido;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $documento;

    /**
     * @var Paretesco
     */
    private $parentesco;

    /**
     * @var \DateTime
     */
    private $fechaBaja;

    /**
     * @var Persona
     */
    private $persona;

    /**
     * @var Titular
     */
    private $titular;

    /**
     * @var
     */
    private $coberturasAdicionales;
    
    /**
     * Set numeroTitular
     *
     * @param integer $numeroTitular
     * @return Miembro
     */
    public function setNumeroTitular($numeroTitular)
    {
        $this->numeroTitular = $numeroTitular;

        return $this;
    }

    /**
     * Get numeroTitular
     *
     * @return integer 
     */
    public function getNumeroTitular()
    {
        return $this->numeroTitular;
    }

    /**
     * Set codigoOrganizacion
     *
     * @param integer $codigoOrganizacion
     * @return Miembro
     */
    public function setCodigoOrganizacion($codigoOrganizacion)
    {
        $this->codigoOrganizacion = $codigoOrganizacion;

        return $this;
    }

    /**
     * Get codigoOrganizacion
     *
     * @return integer 
     */
    public function getCodigoOrganizacion()
    {
        return $this->codigoOrganizacion;
    }

    /**
     * Set numeroMiembro
     *
     * @param integer $numeroMiembro
     * @return Miembro
     */
    public function setNumeroMiembro($numeroMiembro)
    {
        $this->numeroMiembro = $numeroMiembro;

        return $this;
    }

    /**
     * Get numeroMiembro
     *
     * @return integer 
     */
    public function getNumeroMiembro()
    {
        return $this->numeroMiembro;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return Miembro
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Miembro
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set documento
     *
     * @param string $documento
     * @return Miembro
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;

        return $this;
    }

    /**
     * Get documento
     *
     * @return string 
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * Set Parentesco
     *
     * @param Parentesco $parentesco
     * @return Miembro
     */
    public function setParentesco($parentesco)
    {
        $this->parentesco = $parentesco;

        return $this;
    }

    /**
     * Get parentesco
     *
     * @return string 
     */
    public function getParentesco()
    {
        return $this->parentesco;
    }

    /**
     * Set fechaBaja
     *
     * @param \DateTime $fechaBaja
     * @return Miembro
     */
    public function setFechaBaja($fechaBaja)
    {
        $this->fechaBaja = $fechaBaja;

        return $this;
    }

    /**
     * Get fechaBaja
     *
     * @return \DateTime 
     */
    public function getFechaBaja()
    {
        return $this->fechaBaja;
    }

    /**
    *
    * Controla si el miembro es titular
    *
    */
    public function esTitular()
    {
        // Si es titular (Codigo = TI), retorna verdadero
        if(0 == strcmp($this->parentesco->getCodigo(), "TI"))
            return true;
        return false;
    }

    /**
     * Set Persona
     *
     * @param Persona $persona
     * @return Miembro
     */
    public function setPersona($persona)
    {
        $this->persona = $persona;

        return $this;
    }

    /**
     * Get persona
     *
     * @return string 
     */
    public function getPersona()
    {
        return $this->persona;
    }


     /**
     * Set Titular
     *
     * @param Titular $titular
     * @return Miembro
     */
    public function setTitular($titular)
    {
        $this->persona = $titular;

        return $this;
    }

    /**
     * Get titular
     *
     * @return Titular 
     */
    public function getTitular()
    {
        return $this->titular;
    }

    /**
     * Set coberturasAdicionales
     *
     * @param  $coberturasAdicionales
     * @return Miembro
     */
    public function setCoberturasAdicionales($coberturasAdicionales)
    {
        $this->persona = $coberturasAdicionales;

        return $this;
    }

    /**
     * Get coberturasAdicionales
     *
     * @return 
     */
    public function getCoberturasAdicionales()
    {
        return $this->coberturasAdicionales;
    }
}
