<?php

namespace IAPOS\GeneralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Nomenclador
 */
class PracticaNomenclador
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $codigoSeccion;

    /**
     * @var int
     */
    private $codigoCapitulo;

    /**
     * @var string
     */
    private $codigoSubCapitulo;

    /**
     * @var int
     */
    private $codigoSecuencia;

    /**
     * @var int
     */
    private $codigoSubItem;

    /**
     * @var int
     */
    private $codigoOrigen;

    /**
     * @var string
     */
    private $descripcion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigoSeccion
     *
     * @param integer $codigoSeccion
     * @return Nomenclador
     */
    public function setCodigoSeccion($codigoSeccion)
    {
        $this->codigoSeccion = $codigoSeccion;

        return $this;
    }

    /**
     * Get codigoSeccion
     *
     * @return integer 
     */
    public function getCodigoSeccion()
    {
        return $this->codigoSeccion;
    }

    /**
     * Set codigoCapitulo
     *
     * @param integer $codigoCapitulo
     * @return Nomenclador
     */
    public function setCodigoCapitulo($codigoCapitulo)
    {
        $this->codigoCapitulo = $codigoCapitulo;

        return $this;
    }

    /**
     * Get codigoCapitulo
     *
     * @return integer 
     */
    public function getCodigoCapitulo()
    {
        return $this->codigoCapitulo;
    }

    /**
     * Set codigoSubCapitulo
     *
     * @param string $codigoSubCapitulo
     * @return Nomenclador
     */
    public function setCodigoSubCapitulo($codigoSubCapitulo)
    {
        $this->codigoSubCapitulo = $codigoSubCapitulo;

        return $this;
    }

    /**
     * Get codigoSubCapitulo
     *
     * @return string 
     */
    public function getCodigoSubCapitulo()
    {
        return $this->codigoSubCapitulo;
    }

    /**
     * Set codigoSecuencia
     *
     * @param integer $codigoSecuencia
     * @return Nomenclador
     */
    public function setCodigoSecuencia($codigoSecuencia)
    {
        $this->codigoSecuencia = $codigoSecuencia;

        return $this;
    }

    /**
     * Get codigoSecuencia
     *
     * @return integer 
     */
    public function getCodigoSecuencia()
    {
        return $this->codigoSecuencia;
    }

    /**
     * Set codigoSubItem
     *
     * @param integer $codigoSubItem
     * @return Nomenclador
     */
    public function setCodigoSubItem($codigoSubItem)
    {
        $this->codigoSubItem = $codigoSubItem;

        return $this;
    }

    /**
     * Get codigoSubItem
     *
     * @return integer 
     */
    public function getCodigoSubItem()
    {
        return $this->codigoSubItem;
    }

    /**
     * Set codigoOrigen
     *
     * @param integer $codigoOrigen
     * @return Nomenclador
     */
    public function setCodigoOrigen($codigoOrigen)
    {
        $this->codigoOrigen = $codigoOrigen;

        return $this;
    }

    /**
     * Get codigoOrigen
     *
     * @return integer 
     */
    public function getCodigoOrigen()
    {
        return $this->codigoOrigen;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Nomenclador
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}
