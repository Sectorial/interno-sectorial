<?php

namespace IAPOS\GeneralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Titular
 */
class Titular
{
    /**
     * @var int
     */
    private $numero;

    /**
     * @var int
     */
    private $codigoOrganizacion;

    /**
     * @var \DateTime
     */
    private $fechaAlta;

    /**
     * @var \DateTime
     */
    private $fechaBaja;

    /**
    *
    * @var 
    */
    private $clavesDePago;


   /**
     * Set numero
     *
     * @param integer $numero
     * @return Titular
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set codigoOrganizacion
     *
     * @param integer $codigoOrganizacion
     * @return Titular
     */
    public function setCodigoOrganizacion($codigoOrganizacion)
    {
        $this->codigoOrganizacion = $codigoOrganizacion;

        return $this;
    }

    /**
     * Get codigoOrganizacion
     *
     * @return integer 
     */
    public function getCodigoOrganizacion()
    {
        return $this->codigoOrganizacion;
    }

    /**
     * Set fechaAlta
     *
     * @param \DateTime $fechaAlta
     * @return Titular
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    /**
     * Get fechaAlta
     *
     * @return \DateTime 
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    /**
     * Set fechaBaja
     *
     * @param \DateTime $fechaBaja
     * @return Titular
     */
    public function setFechaBaja($fechaBaja)
    {
        $this->fechaBaja = $fechaBaja;

        return $this;
    }

    /**
     * Get fechaBaja
     *
     * @return \DateTime 
     */
    public function getFechaBaja()
    {
        return $this->fechaBaja;
    }


    /**
     * Set clavesDePago
     *
     * @param  $clavesDePago
     * @return Titular
     */
    public function setClavesDePago($clavesDePago)
    {
        $this->clavesDePago = $clavesDePago;

        return $this;
    }

    /**
     * Get clavesDePago
     *
     * @return  
     */
    public function getClavesDePago()
    {
        return $this->clavesDePago;
    }
}
