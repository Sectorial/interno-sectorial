<?php

namespace IAPOS\GeneralBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;

/**
 * UsuarioInterno
 */
class UsuarioInterno extends BaseUser
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $area;


     public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Set area
     *
     * @param string $area
     *
     * @return UsuarioInterno
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }
}

