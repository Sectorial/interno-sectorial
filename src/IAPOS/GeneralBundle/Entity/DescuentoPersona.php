<?php

namespace IAPOS\GeneralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DescuentoPersona
 */
class DescuentoPersona
{
    /**
     * @var int
     */
    private $anio;

    /**
     * @var int
     */
    private $mes;

    /**
     * @var string
     */
    private $apellidoNombre;

    /**
     * @var string
     */
    private $documentoPersona;

    /**
     * @var string
     */
    private $bITILCODIG;

    /**
     * @var int
     */
    private $bIRHCLIQNU;

    /**
     * @var Descuento
     */
    private $descuento;

    /**
     * @var string
     */
    private $origen;

    /**
     * @var string
     */
    private $bIRHCTIFCO;

    /**
     * @var int
     */
    private $bIRHDSECUE;

    /**
     * @var float
     */
    private $porcentaje;

    /**
     * @var float
     */
    private $importe;

    /**
     * @var smallint
     */
    private $bISISCODIG;

    /**
     * Set descuento
     *
     * @param Descuento $descuento
     * @return DescuentoPersona
     */
   public function setDescuento($descuento)
   {
        $this->descuento = $descuento;

        return $this;
   }

   /**
     * Get descuento
     *
     * @return Descuento 
     */
   public function getDescuento()
   {
        return $this->descuento;
   }

    /**
     * Set anio
     *
     * @param integer $anio
     * @return DescuentoPersona
     */
    public function setAnio($anio)
    {
        $this->anio = $anio;

        return $this;
    }

    /**
     * Get anio
     *
     * @return integer 
     */
    public function getAnio()
    {
        return $this->anio;
    }

    /**
     * Set mes
     *
     * @param integer $mes
     * @return DescuentoPersona
     */
    public function setMes($mes)
    {
        $this->mes = $mes;

        return $this;
    }

    /**
     * Get mes
     *
     * @return integer 
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * Set apellidoNombre
     *
     * @param string $apellidoNombre
     * @return DescuentoPersona
     */
    public function setApellidoNombre($apellidoNombre)
    {
        $this->apellidoNombre = $apellidoNombre;

        return $this;
    }

    /**
     * Get apellidoNombre
     *
     * @return string 
     */
    public function getApellidoNombre()
    {
        return $this->apellidoNombre;
    }

     /**
     * Set documentoAfiliado
     *
     * @param string $documentoPersona
     * @return DescuentoPersona
     */
    public function setDocumentoPersona($documentoPersona)
    {
        $this->documentoPersona = $documentoPersona;

        return $this;
    }

    /**
     * Get documentoPersona
     *
     * @return string 
     */
    public function getDocumentoPersona()
    {
        return $this->documentoPersona;
    }


    /**
     * Set bITILCODIG
     *
     * @param string $bITILCODIG
     * @return DescuentoPersona
     */
    public function setBITILCODIG($bITILCODIG)
    {
        $this->bITILCODIG = $bITILCODIG;

        return $this;
    }

   
    /**
     * Get bITILCODIG
     *
     * @return string 
     */
    public function getBITILCODIG()
    {
        return $this->bITILCODIG;
    }

    /**
     * Set bIRHCLIQNU
     *
     * @param integer $bIRHCLIQNU
     * @return DescuentoPersona
     */
    public function setBIRHCLIQNU($bIRHCLIQNU)
    {
        $this->bIRHCLIQNU = $bIRHCLIQNU;

        return $this;
    }

    /**
     * Get bIRHCLIQNU
     *
     * @return integer 
     */
    public function getBIRHCLIQNU()
    {
        return $this->bIRHCLIQNU;
    }

   

    

    

    /**
     * Set origen
     *
     * @param string $origen
     * @return DescuentoPersona
     */
    public function setOrigen($origen)
    {
        $this->origen = $origen;

        return $this;
    }

    /**
     * Get origen
     *
     * @return string 
     */
    public function getOrigen()
    {
        return $this->origen;
    }

    /**
     * Set bIRHCTIFCO
     *
     * @param string $bIRHCTIFCO
     * @return DescuentoPersona
     */
    public function setBIRHCTIFCO($bIRHCTIFCO)
    {
        $this->bIRHCTIFCO = $bIRHCTIFCO;

        return $this;
    }

    /**
     * Get bIRHCTIFCO
     *
     * @return string 
     */
    public function getBIRHCTIFCO()
    {
        return $this->bIRHCTIFCO;
    }

    /**
     * Set bIRHDSECUE
     *
     * @param integer $bIRHDSECUE
     * @return DescuentoPersona
     */
    public function setBIRHDSECUE($bIRHDSECUE)
    {
        $this->bIRHDSECUE = $bIRHDSECUE;

        return $this;
    }

    /**
     * Get bIRHDSECUE
     *
     * @return integer 
     */
    public function getBIRHDSECUE()
    {
        return $this->bIRHDSECUE;
    }

    /**
     * Set porcentaje
     *
     * @param float $porcentaje
     * @return DescuentoPersona
     */
    public function setPorcentaje($porcentaje)
    {
        $this->porcentaje = $porcentaje;

        return $this;
    }

    /**
     * Get porcentaje
     *
     * @return float 
     */
    public function getPorcentaje()
    {
        return $this->porcentaje;
    }

    /**
     * Set importe
     *
     * @param float $importe
     * @return DescuentoPersona
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return float 
     */
    public function getImporte()
    {
        return $this->importe;
    }
}
