<?php

namespace IAPOS\GeneralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HistorialAutorizacionPractica
 */
class HistorialAutorizacionPractica
{
    /**
     * @var int
     */
    private $numeroDelegacion;

    /**
     * @var int
     */
    private $numeroAutorizacion;

    /**
     * @var int
     */
    private $numeroPracticaAutorizacion;

    /**
     * @var int
     */
    private $numeroSecuencia;

    /**
     * @var string
     */
    private $marcaAutorizado;

    /**
     * @var string
     */
    private $tipoAuditoria;

    /**
     * @var \DateTime
     */
    private $fecha;

    /**
     * @var Usuario
     */
    private $usuario;

    /**
     * @var Delegacion
     */
    private $delegacion;

    /**
    * @var AutorizacionPractica
    */
    private $autorizacionPractica;


    

    /**
     * Set numeroDelegacion
     *
     * @param integer $numeroDelegacion
     * @return HistorialAutorizacionPractica
     */
    public function setNumeroDelegacion($numeroDelegacion)
    {
        $this->numeroDelegacion = $numeroDelegacion;

        return $this;
    }

    /**
     * Get numeroDelegacion
     *
     * @return integer 
     */
    public function getNumeroDelegacion()
    {
        return $this->numeroDelegacion;
    }

    /**
     * Set numeroAutorizacion
     *
     * @param integer $numeroAutorizacion
     * @return HistorialAutorizacionPractica
     */
    public function setNumeroAutorizacion($numeroAutorizacion)
    {
        $this->numeroAutorizacion = $numeroAutorizacion;

        return $this;
    }

    /**
     * Get numeroAutorizacion
     *
     * @return integer 
     */
    public function getNumeroAutorizacion()
    {
        return $this->numeroAutorizacion;
    }

    /**
     * Set numeroPracticaAutorizacion
     *
     * @param integer $numeroPracticaAutorizacion
     * @return HistorialAutorizacionPractica
     */
    public function setNumeroPracticaAutorizacion($numeroPracticaAutorizacion)
    {
        $this->numeroPracticaAutorizacion = $numeroPracticaAutorizacion;

        return $this;
    }

    /**
     * Get numeroPracticaAutorizacion
     *
     * @return integer 
     */
    public function getNumeroPracticaAutorizacion()
    {
        return $this->numeroPracticaAutorizacion;
    }

    /**
     * Set numeroSecuencia
     *
     * @param integer $numeroSecuencia
     * @return HistorialAutorizacionPractica
     */
    public function setNumeroSecuencia($numeroSecuencia)
    {
        $this->numeroSecuencia = $numeroSecuencia;

        return $this;
    }

    /**
     * Get numeroSecuencia
     *
     * @return integer 
     */
    public function getNumeroSecuencia()
    {
        return $this->numeroSecuencia;
    }

    /**
     * Set marcaAutorizado
     *
     * @param string $marcaAutorizado
     * @return HistorialAutorizacionPractica
     */
    public function setMarcaAutorizado($marcaAutorizado)
    {
        $this->marcaAutorizado = $marcaAutorizado;

        return $this;
    }

    /**
     * Get marcaAutorizado
     *
     * @return string 
     */
    public function getMarcaAutorizado()
    {
        return $this->marcaAutorizado;
    }

    /**
     * Set tipoAuditoria
     *
     * @param string $tipoAuditoria
     * @return HistorialAutorizacionPractica
     */
    public function setTipoAuditoria($tipoAuditoria)
    {
        $this->tipoAuditoria = $tipoAuditoria;

        return $this;
    }

    /**
     * Get tipoAuditoria
     *
     * @return string 
     */
    public function getTipoAuditoria()
    {
        return $this->tipoAuditoria;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return HistorialAutorizacionPractica
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set usuario
     *
     * @param Usuario $usuario
     * @return HistorialAutorizacionPractica
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return Usuario 
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set delegacion
     *
     * @param Delegacion $delegacion
     * @return HistorialAutorizacionPractica
     */
    public function setDelegacion($delegacion)
    {
        $this->delegacion = $delegacion;

        return $this;
    }

    /**
     * Get delegacion
     *
     * @return Delegacion 
     */
    public function getDelegacion()
    {
        return $this->delegacion;
    }

    /**
     * Set autorizacionPractica
     *
     * @param AutorizacionPractica $autorizacionPractica
     * @return HistorialAutorizacionPractica
     */
    public function setAutorizacionPractica($autorizacionPractica)
    {
        $this->autorizacionPractica = $autorizacionPractica;

        return $this;
    }

    /**
     * Get autorizacionPractica
     *
     * @return AutorizacionPractica 
     */
    public function getAutorizacionPractica()
    {
        return $this->autorizacionPractica;
    }

    public function getEstado()
    {
        $estado;
        
        // Diferido
        if(0 == strcmp($this->getMarcaAutorizado(),"DIF"))
            $estado = "DIFERIDO - ";
        // Autorizado
        else
            $estado = "AUTORIZADO - ";   

        // Tipo de auditoria administrativa
        if(0 == strcmp($this->getTipoAuditoria(),"T"))
            $estado = $estado . "ADMINISTRATIVO";
        // Tipo de auditoria médica ( == D)        
        else
            $estado = $estado . "MÉDICO";
        
        return $estado;
    }
}
