<?php

namespace IAPOS\GeneralBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AutorizacionCabecera
 */
class AutorizacionCabecera
{
    
    /**
     * @var int
     */
    private $numeroDelegacion;

    /**
     * @var int
     */
    private $codigoOrganizacion;

    /**
     * @var int
     */
    private $numeroAutorizacion;

    /**
     * @var string
     */
    private $documentoAfiliado;

    /**
     * @var int
     */
    private $numeroMiembro;

    /**
     * @var int
     */
    private $numeroTitular;

    /**
     * @var int
     */
    private $codigoCobertura;

    /**
     * @var int
     */
    private $numeroProveedor;

    /**
     * @var int
     */
    private $numeroSucursalProveedor;

    /**
     * @var int
     */
    private $numeroResponsable;

    /**
     * @var \DateTime
     */
    private $fechaSolicitud;

    /**
     * @var \DateTime
     */
    private $fechaPrescripcion;

    /**
     * @var string
     */
    private $marcaAutorizado;

    /**
     * @var string
     */
    private $marcaAuditado;

    /**
     * @var \DateTime
     */
    private $fechaAutorizado;

    /**
     * @var \DateTime
     */
    private $fechaVencimiento;

    /**
     * @var \DateTime
     */
    private $fechaAuditado;

    /**
     * @var \DateTime
     */
    private $fechaHoraAlta;

    /**
     * @var Usuario
     */
    private $usuarioAlta;

    /**
     * @var string
     */
    private $esAutorizacion;

    /**
     * @var string
     */
    private $esConsumo;

    /**
     * @var \DateTime
     */
    private $fechaAnulacion;

    /**
     * @var Usuario
     */
    private $usuarioAnulacion;

    /**
     * @var Cobertura
     */
    private $cobertura;

    /**
     * @var Miembro
     */
    private $miembro; 


    /**
     * @var Delegacion
     */
    private $delegacion;

    /**
     * @var Array de AutorizacionPractica
     */
    private $practicas;




       /**
     * Set numeroDelegacion
     *
     * @param integer $numeroDelegacion
     * @return AutorizacionCabecera
     */
    public function setNumeroDelegacion($numeroDelegacion)
    {
        $this->numeroDelegacion = $numeroDelegacion;

        return $this;
    }

    /**
     * Get numeroDelegacion
     *
     * @return integer 
     */
    public function getNumeroDelegacion()
    {
        return $this->numeroDelegacion;
    }
       /**
     * Set codigoOrganizacion
     *
     * @param integer $codigoOrganizacion
     * @return AutorizacionCabecera
     */
    public function setCodigoOrganizacion($codigoOrganizacion)
    {
        $this->codigoOrganizacion = $codigoOrganizacion;

        return $this;
    }

    /**
     * Get codigoOrganizacion
     *
     * @return integer 
     */
    public function getCodigoOrganizacion()
    {
        return $this->codigoOrganizacion;
    }

    /**
     * Set numeroAutorizacion
     *
     * @param integer $numeroAutorizacion
     * @return AutorizacionCabecera
     */
    public function setNumeroAutorizacion($numeroAutorizacion)
    {
        $this->numeroAutorizacion = $numeroAutorizacion;

        return $this;
    }

    /**
     * Get numeroAutorizacion
     *
     * @return integer 
     */
    public function getNumeroAutorizacion()
    {
        return $this->numeroAutorizacion;
    }

    /**
     * Set documentoAfiliado
     *
     * @param string $documentoAfiliado
     * @return AutorizacionCabecera
     */
    public function setDocumentoAfiliado($documentoAfiliado)
    {
        $this->documentoAfiliado = $documentoAfiliado;

        return $this;
    }

    /**
     * Get documentoAfiliado
     *
     * @return string 
     */
    public function getDocumentoAfiliado()
    {
        return $this->documentoAfiliado;
    }

    /**
     * Set numeroMiembro
     *
     * @param integer $numeroMiembro
     * @return AutorizacionCabecera
     */
    public function setNumeroMiembro($numeroMiembro)
    {
        $this->numeroMiembro = $numeroMiembro;

        return $this;
    }

    /**
     * Get numeroMiembro
     *
     * @return integer 
     */
    public function getNumeroMiembro()
    {
        return $this->numeroMiembro;
    }

    /**
     * Set numeroTitular
     *
     * @param integer $numeroTitular
     * @return AutorizacionCabecera
     */
    public function setNumeroTitular($numeroTitular)
    {
        $this->numeroTitular = $numeroTitular;

        return $this;
    }

    /**
     * Get numeroTitular
     *
     * @return integer 
     */
    public function getNumeroTitular()
    {
        return $this->numeroTitular;
    }

    /**
     * Set codigoCobertura
     *
     * @param integer $codigoCobertura
     * @return AutorizacionCabecera
     */
    public function setCodigoCobertura($codigoCobertura)
    {
        $this->codigoCobertura = $codigoCobertura;

        return $this;
    }

    /**
     * Get codigoCobertura
     *
     * @return integer 
     */
    public function getCodigoCobertura()
    {
        return $this->codigoCobertura;
    }
    /**
     * Set delegacion
     *
     * @param Delegacion $delegacion
     * @return AutorizacionCabecera
     */
    public function setDelegacion($delegacion)
    {
        $this->delegacion = $delegacion;

        return $this;
    }

    /**
     * Get delegacion
     *
     * @return Delegacion 
     */
    public function getDelegacion()
    {
        return $this->delegacion;
    }

    /**
     * Set numeroProveedor
     *
     * @param integer $numeroProveedor
     * @return AutorizacionCabecera
     */
    public function setNumeroProveedor($numeroProveedor)
    {
        $this->numeroProveedor = $numeroProveedor;

        return $this;
    }

    /**
     * Get numeroProveedor
     *
     * @return integer 
     */
    public function getNumeroProveedor()
    {
        return $this->numeroProveedor;
    }

    /**
     * Set numeroSucursalProveedor
     *
     * @param integer $numeroSucursalProveedor
     * @return AutorizacionCabecera
     */
    public function setNumeroSucursalProveedor($numeroSucursalProveedor)
    {
        $this->numeroSucursalProveedor = $numeroSucursalProveedor;

        return $this;
    }

    /**
     * Get numeroSucursalProveedor
     *
     * @return integer 
     */
    public function getNumeroSucursalProveedor()
    {
        return $this->numeroSucursalProveedor;
    }

    /**
     * Set numeroResponsable
     *
     * @param integer $numeroResponsable
     * @return AutorizacionCabecera
     */
    public function setNumeroResponsable($numeroResponsable)
    {
        $this->numeroResponsable = $numeroResponsable;

        return $this;
    }

    /**
     * Get numeroResponsable
     *
     * @return integer 
     */
    public function getNumeroResponsable()
    {
        return $this->numeroResponsable;
    }

    /**
     * Set fechaSolicitud
     *
     * @param \DateTime $fechaSolicitud
     * @return AutorizacionCabecera
     */
    public function setFechaSolicitud($fechaSolicitud)
    {
        $this->fechaSolicitud = $fechaSolicitud;

        return $this;
    }

    /**
     * Get fechaSolicitud
     *
     * @return \DateTime 
     */
    public function getFechaSolicitud()
    {
        return $this->fechaSolicitud;
    }

    /**
     * Set fechaPrescripcion
     *
     * @param \DateTime $fechaPrescripcion
     * @return AutorizacionCabecera
     */
    public function setFechaPrescripcion($fechaPrescripcion)
    {
        $this->fechaPrescripcion = $fechaPrescripcion;

        return $this;
    }

    /**
     * Get fechaPrescripcion
     *
     * @return \DateTime 
     */
    public function getFechaPrescripcion()
    {
        return $this->fechaPrescripcion;
    }

    /**
     * Set marcaAutorizado
     *
     * @param string $marcaAutorizado
     * @return AutorizacionCabecera
     */
    public function setMarcaAutorizado($marcaAutorizado)
    {
        $this->marcaAutorizado = $marcaAutorizado;

        return $this;
    }

    /**
     * Get marcaAutorizado
     *
     * @return string 
     */
    public function getMarcaAutorizado()
    {
        return $this->marcaAutorizado;
    }

    /**
     * Set marcaAuditado
     *
     * @param string $marcaAuditado
     * @return AutorizacionCabecera
     */
    public function setMarcaAuditado($marcaAuditado)
    {
        $this->marcaAuditado = $marcaAuditado;

        return $this;
    }

    /**
     * Get marcaAuditado
     *
     * @return string 
     */
    public function getMarcaAuditado()
    {
        return $this->marcaAuditado;
    }

    /**
     * Set fechaAutorizado
     *
     * @param \DateTime $fechaAutorizado
     * @return AutorizacionCabecera
     */
    public function setFechaAutorizado($fechaAutorizado)
    {
        $this->fechaAutorizado = $fechaAutorizado;

        return $this;
    }

    /**
     * Get fechaAutorizado
     *
     * @return \DateTime 
     */
    public function getFechaAutorizado()
    {
        return $this->fechaAutorizado;
    }

    /**
     * Set fechaVencimiento
     *
     * @param \DateTime $fechaVencimiento
     * @return AutorizacionCabecera
     */
    public function setFechaVencimiento($fechaVencimiento)
    {
        $this->fechaVencimiento = $fechaVencimiento;

        return $this;
    }

    /**
     * Get fechaVencimiento
     *
     * @return \DateTime 
     */
    public function getFechaVencimiento()
    {
        return $this->fechaVencimiento;
    }

    /**
     * Set fechaAuditado
     *
     * @param \DateTime $fechaAuditado
     * @return AutorizacionCabecera
     */
    public function setFechaAuditado($fechaAuditado)
    {
        $this->fechaAuditado = $fechaAuditado;

        return $this;
    }

    /**
     * Get fechaAuditado
     *
     * @return \DateTime 
     */
    public function getFechaAuditado()
    {
        return $this->fechaAuditado;
    }

    /**
     * Set fechaHoraAlta
     *
     * @param \DateTime $fechaHoraAlta
     * @return AutorizacionCabecera
     */
    public function setFechaHoraAlta($fechaHoraAlta)
    {
        $this->fechaHoraAlta = $fechaHoraAlta;

        return $this;
    }

    /**
     * Get fechaHoraAlta
     *
     * @return \DateTime 
     */
    public function getFechaHoraAlta()
    {
        return $this->fechaHoraAlta;
    }

    /**
     * Set usuarioAlta
     *
     * @param Usuario $usuarioAlta
     * @return AutorizacionCabecera
     */
    public function setUsuarioAlta($usuarioAlta)
    {
        $this->usuarioAlta = $usuarioAlta;

        return $this;
    }

    /**
     * Get usuarioAlta
     *
     * @return Usuario 
     */
    public function getUsuarioAlta()
    {
        return $this->usuarioAlta;
    }

    /**
     * Set esAutorizacion
     *
     * @param string $esAutorizacion
     * @return AutorizacionCabecera
     */
    public function setEsAutorizacion($esAutorizacion)
    {
        $this->esAutorizacion = $esAutorizacion;

        return $this;
    }

    /**
     * Get esAutorizacion
     *
     * @return string 
     */
    public function getEsAutorizacion()
    {
        return $this->esAutorizacion;
    }

    /**
     * Set esConsumo
     *
     * @param string $esConsumo
     * @return AutorizacionCabecera
     */
    public function setEsConsumo($esConsumo)
    {
        $this->esConsumo = $esConsumo;

        return $this;
    }

    /**
     * Get esConsumo
     *
     * @return string 
     */
    public function getEsConsumo()
    {
        return $this->esConsumo;
    }

    /**
     * Set fechaAnulacion
     *
     * @param \DateTime $fechaAnulacion
     * @return AutorizacionCabecera
     */
    public function setFechaAnulacion($fechaAnulacion)
    {
        $this->fechaAnulacion = $fechaAnulacion;

        return $this;
    }

    /**
     * Get fechaAnulacion
     *
     * @return \DateTime 
     */
    public function getFechaAnulacion()
    {
        return $this->fechaAnulacion;
    }

    /**
     * Set usuarioAnulacion
     *
     * @param Usuario $usuarioAnulacion
     * @return AutorizacionCabecera
     */
    public function setUsuarioAnulacion($usuarioAnulacion)
    {
        $this->usuarioAnulacion = $usuarioAnulacion;

        return $this;
    }

    /**
     * Get usuarioAnulacion
     *
     * @return Usuario 
     */
    public function getUsuarioAnulacion()
    {
        return $this->usuarioAnulacion;
    }
    
    /**
     * Set cobertura
     *
     * @param Cobertura $cobertura
     * @return AutorizacionCabecera
     */
    public function setCobertura($cobertura)
    {
        $this->cobertura = $cobertura;

        return $this;
    }

    /**
     * Get cobertura
     *
     * @return Cobertura 
     */
    public function getCobertura()
    {
        return $this->cobertura;
    }

    /**
     * Set miembro
     *
     * @param Miembro $miembro
     * @return AutorizacionCabecera
     */
    public function setMiembro($miembro)
    {
        $this->miembro = $miembro;

        return $this;
    }

    /**
     * Get miembro
     *
     * @return Miembro 
     */
    public function getMiembro()
    {
        return $this->miembro;
    }

    /**
     * Get modalidad
     *
     * @return string 
     */
    public function getModalidad()
    {
        if($this->getEsConsumo() == 'S' and $this->getEsAutorizacion() == 'S')
            return "Autorización y Consumo Simultáneo";
        else if ($this->getEsConsumo() == 'S')
            return "Consumo";
        else
            return "Autorización";
    }
    

    /**
     * Set practicas
     *
     * @param Array $practicas
     * @return AutorizacionCabecera
     */
    public function setPracticas($practicas)
    {
        $this->practicas = $practicas;

        return $this;
    }

    /**
     * Get practicas
     *
     * @return Array 
     */
    public function getPracticas()
    {
        return $this->practicas;
    }

    public function getEstaAnulada(){
        if(0 == strcmp($this->getFechaAnulacion()->format("Y"), '0001'))
            return false;
        return true;
    }

    // Devuelve el estado de la autorizacion
    public function getEstado(){
        if(0 == strcmp($this->getMarcaAuditado(),"DIF") && 0 == strcmp($this->getMarcaAutorizado(),"DIF"))
            return "DIFERIDO MED y ADM";
        elseif(0 == strcmp($this->getMarcaAuditado(),"DIF"))
            return "DIFERIDO MED";
        elseif(0 == strcmp($this->getMarcaAutorizado(),"DIF"))
            return "DIFERIDO ADM";
        else if(0 == sizeof($this->getPracticas()))
            return "SIN DEFINIR";
        else
            return "AUTORIZADO";  
    }
}
